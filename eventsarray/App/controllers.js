define(['application'],
    function (application) {
        application.controller('appController', ['$scope', function ($scope) {
            $scope.data = 'Hello!';
            $scope.count = 500;
            $scope.message = "From App Controller";
        }]);
        console.log("[OK] - Loading - appController");
    }
);

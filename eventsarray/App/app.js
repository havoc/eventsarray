var deps = ['angular', 'app/namespace', 'angularRoute', 'routeResolver', 'ionicAngular'];
define(deps, function (angular, namespace) {
    'use strict';

    var application = angular.module(namespace, ['ionic', 'ui.router', 'routeResolverServices']);

    // Helper function to resolve relative paths to absolute paths
    application.resolveAssetUrl = function (ns, relativePath) {
        ns = ns.replace(new RegExp("\\.", "g"), '/');
        var path = ns + "/" + relativePath;
        return path.replace(new RegExp("//", "g"), '/');
    };

    function resolveModuleName(ns, moduleName, moduleType) {
        var parts = ns.split(".");
        moduleName = moduleName || (parts[parts.length - 1] + moduleType);
        return (parts.length > 1) ? ns + "." + moduleName : moduleName;
    }

    application.resolveControllerName = function (ns, controllerName) {
        return resolveModuleName(ns, controllerName, "Controller");
    };

    application.resolveServiceName = function (ns, serviceName) {
        return resolveModuleName(ns, serviceName, "Service");
    };
    
    application.config(['$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
        function ($controllerProvider, $compileProvider, $filterProvider, $provide) {

            // Preferred approach is conventions (over configurations)
            // If and only if required, allow setting the base directories for views and controllers
            // Change default views and controllers directory using the following:
            // routeResolverProvider.routeConfig.setBaseDirectories('/app/views', '/app/controllers');

            application._directive = application.directive;

            // Provider-based directive.
            application.directive = function (name, factory) {
                $compileProvider.directive(name, factory);
                return (this);
            };

            application.register =
            {
                controller: $controllerProvider.register,
                directive: $compileProvider.directive,
                filter: $filterProvider.register,
                factory: $provide.factory,
                service: $provide.service
            };
        }
    ]);
    console.log('[OK] - Loading App : ', namespace);
    return application;
});


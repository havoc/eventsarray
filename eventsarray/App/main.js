﻿function printLogo() {
    // Generated using http://fsymbols.com/generators/tarty/
    console.log("█▀▀ █░░█ █▀▀ █░░ █░░       Fork me : https://github.com/ideajoint/shell");
    console.log("▀▀█ █▀▀█ █▀▀ █░░ █░░       Demo    : http://shellapp.azurewebsites.net");
    console.log("▀▀▀ ▀░░▀ ▀▀▀ ▀▀▀ ▀▀▀       Docs    : http://ideajoint.github.io/shell");
}

printLogo();

var isDebugMode = false;
require.config(
    {
        baseUrl: '',
        urlArgs: 'bust=v1' + (isDebugMode ? '.' + (new Date().getTime()) : ''),
        waitSeconds: 0,  //make sure it is enough to load all scripts

        paths: {
            'require': 'app/scripts/requirejs/2.1.14/require.min',
            '$': 'app/scripts/jquery/2.1.1/jquery-2.1.1.min',
            'angular': 'app/scripts/angularjs/1.3.0-rc.0/angular.min',
            'angularAnimate': 'app/scripts/angularjs/1.3.0-rc.0/angular-animate.min',
            'angularSanitize': 'app/scripts/angularjs/1.3.0-rc.0/angular-sanitize.min',
            'angularRoute': 'app/scripts/ui-router/0.2.11/angular-ui-router.min',
            'ionic': 'app/scripts/ionic/1.0.0.beta.12/ionic',
            'ionicAngular': 'app/scripts/ionic/1.0.0.beta.12/ionic-angular.min',
            'domReady': 'app/scripts/requirejs/domReady/2.0.1/domReady',
            'routeResolver': 'app/scripts/shell/utils/routeResolver',
            'application': 'app/app',
            'routeConfig': 'app/scripts/shell/config/routeConfig',
            'loader': 'app/scripts/shell/utils/loader',
            'underscore': 'app/scripts/underscore/1.5.2/lodash.underscore.min'
        },

        // Dependencies
        shim: {
            'angular': { 'exports': 'angular' },
            'angularAnimate': { deps: ['angular'] },
            'angularSanitize': { deps: ['angular'] },
            'angularRoute': { deps: ['angular'] },
            'ionic': { deps: ['angular'], exports: 'ionic' },
            'ionicAngular': { deps: ['angular', 'ionic', 'angularRoute', 'angularAnimate', 'angularSanitize'] },
            'underscore': { 'exports': 'underscore' }
        },

        priority: [
        'angular',
        'ionic'
        ],
    }
);

// Bootstrap App
// http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
//window.name = 'NG_DEFER_BOOTSTRAP!';

var deps = ['require', 'ionic', 'angular', 'application', 'app/namespace', 'routeConfig', 'loader', 'app/controllers'];
define(deps, function (require, ionic, angular, application, namespace) {
    'use strict';
    require(['domReady!'], function (document) {
        try {
            angular.bootstrap(document, [namespace]);
        } catch (e) {
            console.error(e.stack || e.message || e);
        }
        require(['./app/shared/module.require']);
    });
    console.log('[OK] - Bootstrapping App : ', namespace);
});

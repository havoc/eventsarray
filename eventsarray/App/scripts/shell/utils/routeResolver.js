'use strict';

define(['angular'], function(angular){
    var routeResolver = function (){
        this.$get = function() {
            return this;
        }

        this.route = function() {

            var resolveDependencies = function($q, $rootScope, dependencies) {
                var defer = $q.defer();
                require(dependencies, function() {
                    defer.resolve();
                    $rootScope.$apply();
                });

                return defer.promise;
            };

            var resolveController = function(controllerPath) {
                var routeDef = {};
                //routeDef.controller = controllerPath.replace (new RegExp("/", "g"), '.') + 'Controller';
                routeDef.resolve = {
                    load: [
                        '$q', '$rootScope', function($q, $rootScope) {
                            var dependencies = [controllerPath + 'Controller.js'];
                            var resolvedDependencies = resolveDependencies($q, $rootScope, dependencies);
                            return resolvedDependencies;
                        }
                    ]
                }

                return routeDef;
            }

            var resolve = function(url, viewPath, controllerPath) {

                controllerPath = (controllerPath) ? controllerPath : viewPath;

                var routeDef = {};
                routeDef.url = url;
                routeDef.templateUrl = viewPath + ".html";
                routeDef.controller = controllerPath.replace (new RegExp("/", "g"), '.') + 'Controller';
                angular.extend(routeDef, resolveController(controllerPath));

//                routeDef.resolve = {
//                    load: ['$q', '$rootScope', function ($q, $rootScope) {
//                        var dependencies = [controllerPath + 'Controller.js'];
//                        var resolvedDependencies = resolveDependencies($q, $rootScope, dependencies);
//                        return resolvedDependencies;
//                    }]
//                }

                return routeDef;
            };

            return {
                resolveController: resolveController,
                resolve: resolve
            }
        }();
    }

    var servicesApp = angular.module('routeResolverServices', []);

    //Must be a provider since it will be injected into module.config()
    servicesApp.provider('routeResolver', routeResolver);

    console.log('[OK] - Loading RouteResolver');
});
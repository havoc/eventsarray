﻿var deps = ['require'];
define(deps, function (require) {
    var config = {
        'home': [
            './app/components/contacts/_index'
        ]
    };

    var prepare = function(moduleName) {
        var dependents = config[moduleName];
        require(dependents);
    };

    console.log('[OK] - Loading SmartLoader');
    return {
        prepare: prepare
    }

});
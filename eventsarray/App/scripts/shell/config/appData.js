﻿var appData = appData || {};

appData.iconList = [
    {
        name: 'Microsoft',
        templateName: 'app/shared/widgets/logos/Microsoft.html',
        type: 'company',
        searchable: true
    },
    {
        name: 'Apple',
        templateName: 'app/shared/widgets/logos/Apple.html',
        type: 'company',
        searchable: true
    },
    {
        name: 'Google',
        templateName: 'app/shared/widgets/logos/Google.html',
        type: 'company',
        searchable: true
    },
    //{
    //    name: 'Delta',
    //    templateName: 'app/shared/widgets/icons/placeholder.html',
    //    type: 'section',
    //    searchable: false
    //},
    //{
    //    name: 'Theta',
    //    templateName: 'app/shared/widgets/icons/placeholder.html',
    //    type: 'section',
    //    searchable: false
    //}
];


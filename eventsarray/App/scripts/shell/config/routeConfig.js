var deps = ['require', 'application'];
define(deps, function (require, application) {

    var appconfig = application.config(['$stateProvider', '$urlRouterProvider', 'routeResolverProvider',
        function ($stateProvider, $urlRouterProvider, routeResolverProvider) {

            // Route Configuration
            var route = routeResolverProvider.route;

            $urlRouterProvider.otherwise('/');

            $stateProvider
                .state('start', route.resolve('/', 'app/modules/start/start'))
                .state('home', route.resolve('/home', 'app/modules/home/home'));

            // Sample route config with path hierarchy (ex: /contacts and /contacts/new)
            //.state('contacts', {
            //    url: '/contacts',
            //    templateUrl: 'app/modules/contacts/contact.html'
            //})
            //    .state('contacts.new', {
            //        url: '/new',
            //        templateUrl: 'app/modules/contacts/new/new.html'
            //    });

        }
    ]);

});
var deps = ['application', '../namespace.js', 'angular', 'underscore'];
define(deps, function (application, namespace, ng, _) {

    var $ = ng.element;

    /* Iframe extension */

    application.register.directive('shellIframeOnload', [function () {
        return {
            scope: {
                callBack: '&shellIframeOnload'
            },
            link: function (scope, element, attrs) {
                element.on('load', function () {
                    scope.ready = true;
                    return scope.callBack();
                });
            }
        }
    }
    ]);

    /* Nav List and Item */

    application.register.directive('shellNavList', ['$ionicScrollDelegate', function ($ionicScrollDelegate) {
        var _maxIconScrollTop = -1;
        var _iconScrollable = false;
        var _iconScroll;
        return {
            restrict: 'E',
            replace: false,
            transclude: true,
            scope: {
                delegateHandle: '@'
            },
            controller: ['$scope', function shellNavListController($scope) {
                var ctrl = this,
                    previousSelectedItem = ctrl.previousSelectedItem = {};

                ctrl.selectNavItem = function (navItem) {
                    if (previousSelectedItem != {}) {
                        previousSelectedItem.active = false;
                    }
                    navItem.active = true;
                    previousSelectedItem = navItem;
                    navItem.onSelect();
                }

                function init() {
                    $scope.flag = {
                        moreOnTop: false,
                        moreOnBottom: false
                    };
                }

                init();
            }],
            compile: function (element, attrs) {
                _iconScrollable = true;
                var ionContent = $(element[0]).find("ion-content")[0];
                // Dynamically adding ionScrollDelegate handle here - Static Assignment doesn't work
                $(ionContent).attr("delegate-handle", attrs.delegateHandle);
                return function link($scope, element, attrs) {
                    function getScrollDelegate() {
                        _iconScroll = _iconScroll || $ionicScrollDelegate.$getByHandle(attrs.delegateHandle);
                        return _iconScroll;
                    }

                    _iconScroll = _iconScroll || getScrollDelegate();

                    $scope.didIconScroll = function () {
                        var iconScroll = getScrollDelegate();
                        _maxIconScrollTop = _maxIconScrollTop != -1 ? _maxIconScrollTop : iconScroll.getScrollView().getScrollMax().top;
                        var position = iconScroll.getScrollPosition();
                        var defferedAction = function () {
                            $scope.flag.moreOnTop = (position.top > 0) && _iconScrollable;
                            $scope.flag.moreOnBottom = !(position.top >= _maxIconScrollTop) && _iconScrollable;
                        };
                        if (!$scope.$parent.$$phase) {
                            $scope.$apply(function () {
                                defferedAction();
                            });
                        } else {
                            defferedAction();
                        }
                    }

                    $scope.didIconScrollToTopClick = function () {
                        var iconScroll = getScrollDelegate();
                        iconScroll.scrollTop(false);
                    }

                    $scope.didIconScrollToBottomClick = function () {
                        var iconScroll = getScrollDelegate();
                        iconScroll.scrollBottom(false);
                    }
                }
            },
            templateUrl: application.resolveAssetUrl(namespace, 'directives/templates/shellNavList.html')
        };
    }]);

    application.register.directive('shellNavItem', function () {
        return {
            restrict: 'E',
            replace: false,
            require: '^shellNavList',
            transclude: true,
            scope: {
                onSelect: '&'
            },
            compile: function (element, attr) {
                return function link(scope, element, attrs, shellNavListController) {

                    scope.didSelect = function () {
                        shellNavListController.selectNavItem(scope);
                    }
                }
            },
            templateUrl: application.resolveAssetUrl(namespace, 'directives/templates/shellNavItem.html')
        }
    });

    /* Card List & Item */

    application.register.directive('shellCardList', ['$ionicScrollDelegate', '$timeout', function ($ionicScrollDelegate, $timeout) {
        return {
            restrict: 'E',
            replace: false,
            transclude: true,
            scope: {
                delegateHandle: '@',
                focusedMode: '='
            },
            compile: function (element, attrs) {
                var ionContent = $(element[0]).children()[0];
                // Dynamically adding ionScrollDelegate handle here - Static assignment doesn't work
                $(ionContent).attr("delegate-handle", attrs.delegateHandle);
                $(ionContent).find("ul").attr("amscrolling", "{{amscrolling}}");
                return function (scope, element, attrs) {
                    // Empty Link function
                }
            },
            controller: function ($scope) {
                var waitTimeToScroll = 300;

                function getOffsetFromLeft(cardItem) {
                    var index = ctrl.cardList.indexOf(cardItem);
                    var offset = 0;
                    var leftSibilings = ctrl.cardList.slice(0, index);
                    var countOfGroupedCards = _.countBy(leftSibilings, function (item) { return item.cardSize; });
                    offset = (countOfGroupedCards.wide || 0) * window.screen.width +
                                (countOfGroupedCards.narrow || 0) * (window.screen.width / 4) +
                                (countOfGroupedCards.bar || 0) * 75;
                    return offset;
                }

                function scrollTo(offsetToMove, time, animate) {
                    animate = animate != undefined ? animate : true;
                    var delegate = $ionicScrollDelegate.$getByHandle($scope.delegateHandle);
                    delegate.scrollTo(offsetToMove, 0, animate);
                }

                function scrollBy(offsetToMove, time, animate) {
                    animate = animate != undefined ? animate : true;
                    var delegate = $ionicScrollDelegate.$getByHandle($scope.delegateHandle);
                    delegate.scrollBy(offsetToMove, 0, animate);
                }

                var ctrl = this;
                ctrl.cardList = $scope.cardList = [];
                ctrl.focusedCard = false;

                ctrl.addCard = function (cardItem) {
                    ctrl.cardList.push(cardItem);
                }

                ctrl.updateCard = function (cardItem, size) {
                    var index = ctrl.cardList.indexOf(cardItem);
                    cardItem.cardSize = size;
                    //ctrl.cardList[index] = cardItem;
                    console.log(ctrl.cardList);
                }

                ctrl.resizeCard = function (cardItem, size) {
                    var offsetToMove = getOffsetFromLeft(cardItem);
                    //cardItem.cardSize = size;
                    ctrl.updateCard(cardItem, size);
                    $timeout(function () {
                        requestAnimationFrame(function () {
                            scrollTo(offsetToMove);
                        });
                    }, waitTimeToScroll);
                }

                ctrl.deleteCard = function (cardItem) {
                    var index = ctrl.cardList.indexOf(cardItem);
                    ctrl.cardList.splice(index, 1);
                    $timeout(function () {
                        requestAnimationFrame(function () {
                            scrollBy(0);
                        });
                    }, waitTimeToScroll);
                }

                ctrl.focusCard = function (cardItem) {
                    if (ctrl.focusedCard) {
                        ctrl.focusedCard.focused = false;
                    }
                    if (ctrl.focusedCard != cardItem) {
                        cardItem.focused = cardItem.focused ? false : true;
                    }
                    if (cardItem.focused) {
                        ctrl.focusedCard = cardItem;
                        $scope.focusedMode = true;
                    } else {
                        ctrl.focusedCard = false;
                        $scope.focusedMode = false;
                    }
                }
            },
            templateUrl: application.resolveAssetUrl(namespace, 'directives/templates/shellCardList.html')
        };
    }]);

    application.register.factory('$shellBind', [
        '$interpolate', function ($interpolate) {
            return function (scope, attrs, bindings) {
                ng.forEach(bindings || [], function (scopeName) {
                    if (scope[scopeName] === undefined) {
                        scope[scopeName] = attrs[scopeName];
                    }
                });
            }
        }
    ]);

    application.register.directive('shellCardItem', ['$ionicScrollDelegate', '$timeout', '$shellBind', function ($ionicScrollDelegate, $timeout, $shellBind) {
        return {
            restrict: 'E',
            replace: 'element',
            require: '^shellCardList',
            transclude: true,
            scope: {
                onDelete: '&',
                cardIndex: '=?',
                cardSize: '=?',
                cardTitle: '=?',
                cardControls: '=?',
                cardHasBarview: '=?',
                cardHasNarrowview: '=?',
                cardHasWideview: '=?'
            },
            controller: ['$scope', '$element', function ($scope, $element) {
                // Empty Controller
                //console.log("shellCardItem - controller");
            }],
            link: {
                pre: function ($scope, $element, $attrs) {
                    //console.log("shellCardItem - pre link");
                    $shellBind($scope, $attrs, ['cardSize', 'cardTitle', 'cardControls']);

                    $scope.cardSize = $attrs.cardSize = $scope.cardSize != undefined ? $scope.cardSize : 'narrow';
                    $scope.cardIndex = $attrs.cardIndex = $scope.cardIndex != undefined ? $scope.cardIndex : -1;
                    if ($scope.cardControls == $attrs.cardControls) {
                        $scope.cardControls = "true";
                    }
                    $scope.cardControls = $scope.cardControls != undefined ? ($scope.cardControls == 'true' || $scope.cardControls == true) : true;

                    if ($scope.cardHasBarview == $attrs.cardHasBarview) {
                        $scope.cardHasBarview = "false";
                    }
                    $scope.cardHasBarview = $scope.cardHasBarview != undefined ? ($scope.cardHasBarview == 'true' || $scope.cardHasBarview == true) : false;

                    if ($scope.cardHasNarrowview == $attrs.cardHasNarrowview) {
                        $scope.cardHasNarrowview = "true";
                    }
                    $scope.cardHasNarrowview = $scope.cardHasNarrowview != undefined ? ($scope.cardHasNarrowview == 'true' || $scope.cardHasNarrowview == true) : true;

                    if ($scope.cardHasWideview == $attrs.cardHasWideview) {
                        $scope.cardHasWideview = "true";
                    }
                    $scope.cardHasWideview = $scope.cardHasWideview != undefined ? ($scope.cardHasWideview == 'true' || $scope.cardHasWideview == true) : true;

                    if ($scope.cardSize == "bar") {
                        if ($scope.cardHasNarrowview) {
                            $scope.restoreSize = 'narrow';
                        } else {
                            if ($scope.cardHasWideview) $scope.restoreSize = 'wide';
                        }
                    } else {
                        $scope.restoreSize = ng.copy($scope.cardSize);
                    }
                    $scope.focused = false;
                },
                post: function ($scope, $element, $attrs, cardListController) {
                    //console.log("shellCardItem - post link");
                    cardListController.addCard($scope);

                    $scope.didExpandClick = function () {
                        cardListController.resizeCard($scope, 'wide');
                    }

                    $scope.didCollapseClick = function () {
                        cardListController.resizeCard($scope, 'narrow');
                    }

                    $scope.didCloseClick = function () {
                        $scope.onDelete();
                        cardListController.deleteCard($scope);
                    }

                    $scope.didRestoreClick = function () {
                        var restoreSize = ng.copy($scope.restoreSize);
                        cardListController.resizeCard($scope, restoreSize);
                    }

                    $scope.didMinimizeClick = function () {
                        $scope.restoreSize = ng.copy($scope.cardSize);
                        cardListController.resizeCard($scope, 'bar');
                    }

                    $scope.didFocusToggleClick = function () {
                        //$scope.focused = !$scope.focused;
                        cardListController.focusCard($scope);
                    }

                    $scope.printme = function (value) {
                        console.log('printme - ', value);
                    }
                }
            },
            templateUrl: application.resolveAssetUrl(namespace, 'directives/templates/shellCardItem.html')
        };
    }]);

    /* Overlay & Tab */

    application.register.directive('shellOverlay', ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                type: '@',
                showText: '@'
            },
            controller: [
                '$scope', function shellOverlayController($scope) {
                    //console.log('ShellOverlay - Controller');
                    var ctrl = this,
                        tabs = ctrl.tabs = $scope.tabs = [],
                        state = ctrl.state = $scope.state = {};

                    state.tabCount = 0;

                    ctrl.addTab = function addTab(tab) {
                        tabs.push(tab);
                        // we can't run the select function on the first tab
                        // since that would select it twice
                        if (tabs.length === 1) {
                            tab.active = true;
                        } else if (tab.active) {
                            ctrl.select(tab);
                        }
                        state.tabCount = tabs.length;
                    };

                    //ctrl.removeTab = function removeTab(tab) {
                    //    var index = tabs.indexOf(tab);
                    //    //Select a new tab if the tab to be removed is selected
                    //    if (tab.active && tabs.length > 1) {
                    //        //If this is the last tab, select the previous tab. else, the next tab.
                    //        var newActiveIndex = index == tabs.length - 1 ? index - 1 : index + 1;
                    //        ctrl.selectTab(tabs[newActiveIndex]);
                    //    }
                    //    tabs.splice(index, 1);
                    //    state.tabCount = tabs.length;
                    //};

                    var changeActiveTab = $scope.changeActiveTab = function (currentTabId) {
                        if ($scope.state.selectedTabId != currentTabId) {
                            tabs[$scope.state.selectedTabId].active = false;
                            tabs[currentTabId].active = true;
                            $scope.state.selectedTabId = currentTabId;
                        }
                    }

                    ctrl.selectTab = function (selectedTab) {
                        var currentSelectedtabId = tabs.indexOf(selectedTab);
                        $scope.state.mode = 'expanded'; // ($scope.state.mode == 'collapsed') ? 'expanded' : 'collapsed';
                        changeActiveTab(currentSelectedtabId);
                    }
                }
            ],
            link: function (scope, element, attrs) {
                //console.log('ShellOverlay - Link');
                scope.vertical = angular.isDefined(attrs.vertical) ? scope.$parent.$eval(attrs.vertical) : false;
                scope.justified = angular.isDefined(attrs.justified) ? scope.$parent.$eval(attrs.justified) : false;
                scope.state = {
                    mode: 'minimized',
                    selectedTabId: 0,
                    autoplay: false
                }

                scope.didMoveRightClick = function () {
                    var nextTabId = (scope.state.selectedTabId + 1) % scope.tabs.length;
                    scope.changeActiveTab(nextTabId);
                }

                function startAutoPlay() {
                    if (!scope.state.autoplay) return;
                    $timeout(function () {
                        scope.didMoveRightClick();
                        startAutoPlay();
                    }, 2000);
                }

                scope.didAutoPlayToggle = function () {
                    scope.state.autoplay = !scope.state.autoplay;
                    startAutoPlay();
                }

                scope.didExpandClick = function () {
                    scope.state.mode = 'expanded';
                }

                scope.didCollapseClick = function () {
                    scope.state.mode = 'collapsed';
                }

                scope.didHideClick = function () {
                    scope.state.autoplay = false;
                    scope.state.mode = 'minimized';
                }

                scope.didShowClick = function () {
                    scope.state.mode = 'collapsed';
                }

            },
            templateUrl: application.resolveAssetUrl(namespace, 'directives/templates/shellOverlay.html')
        };
    }]);

    application.register.directive('shellTab', [
        '$parse', function ($parse) {
            return {
                require: '^shellOverlay',
                restrict: 'EA',
                replace: true,
                transclude: true,
                scope: {
                    active: '=?',
                    heading: '@',
                    //onSelect: '&select', //This callback is called in contentHeadingTransclude
                    ////once it inserts the tab's content into the dom
                    //onDeselect: '&deselect',
                },
                controller: function () {
                    //console.log('ShellTab - controller : empty');
                    //Empty controller so other directives can require being 'under' a tab
                },
                compile: function (elm, attrs, transclude) {
                    //console.log('ShellTab - compile');
                    return function postLink(scope, elm, attrs, overlayController) {
                        //console.log('ShellTab - postlink');
                        scope.$watch('active', function (active) {
                            if (active) {
                                overlayController.selectTab(scope);
                            }
                        });

                        scope.disabled = false;
                        if (attrs.disabled) {
                            scope.$parent.$watch($parse(attrs.disabled), function (value) {
                                scope.disabled = !!value;
                            });
                        }

                        scope.didTabselect = function () {
                            overlayController.selectTab(scope);
                        };

                        overlayController.addTab(scope);

                        //scope.$on('$destroy', function () {
                        //    overlayController.removeTab(scope);
                        //});

                        //We need to transclude later, once the content container is ready.
                        //when this link happens, we're inside a tab heading.
                        scope.$transcludeFn = transclude;
                    };
                },
                templateUrl: application.resolveAssetUrl(namespace, 'directives/templates/shellTab.html')
            };
        }
    ]);

    application.register.directive('shellTabHeadingTransclude', [
        function () {
            return {
                restrict: 'A',
                require: '^shellTab',
                link: function (scope, elm, attrs, tabCtrl) {
                    //console.log('StabHeadingTransclude - link');
                    scope.$watch('headingElement', function updateHeadingElement(heading) {
                        if (heading) {
                            elm.html('');
                            elm.append(heading);
                        }
                    });
                }
            };
        }
    ]);

    application.register.directive('shellTabContentTransclude', function () {
        return {
            restrict: 'A',
            require: '^shellOverlay',
            link: function (scope, elm, attrs) {
                var tab = scope.$eval(attrs.shellTabContentTransclude);
                //console.log('ShellTabContentTransclude - Link');
                //Now our tab is ready to be transcluded: both the tab heading area
                //and the tab content area are loaded.  Transclude 'em both.
                tab.$transcludeFn(tab.$parent, function (contents) {
                    angular.forEach(contents, function (node) {
                        if (isTabHeading(node)) {
                            //Let tabHeadingTransclude know.
                            tab.headingElement = node;
                        } else {
                            elm.append(node);
                        }
                    });
                });
            }
        };

        function isTabHeading(node) {
            return node.tagName && (
                node.hasAttribute('tab-heading') ||
                    node.hasAttribute('data-tab-heading') ||
                    node.tagName.toLowerCase() === 'tab-heading' ||
                    node.tagName.toLowerCase() === 'data-tab-heading'
            );
        }
    });

    console.log("[OK] - Loading core directives");
});
define([
    '../namespace.js'
], function (parentNamespace) {
    var namespace = "shared";
    return parentNamespace + "." + namespace;
});
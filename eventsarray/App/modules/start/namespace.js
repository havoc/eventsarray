define([
    '../namespace'
], function (parentNamespace) {
    var namespace = "start";
    return parentNamespace + "." + namespace;
});
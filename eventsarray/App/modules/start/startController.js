﻿var deps = ['angular', 'application', './namespace', 'loader'];
define(deps, function (angular, application, namespace, loader) {
    // Loaded shared components
    require(['/./app/shared/module.require.js']);

    var controllerName = application.resolveControllerName(namespace);

    application.register.controller(controllerName, ['$scope', '$window', '$timeout', '$state',
        function controller($scope, $window, $timeout, $state) {
            var isAppReady = false;
            var stayForever = false;
            function _init() {
                var vm = {
                    appName: "EventsArray",
                    counter: 1,
                };
                angular.extend($scope, vm);

                $timeout(function () {
                    _checkIfAppReady();
                }, 1000);
            }
            _init();

            function _checkIfAppReady() {
                $scope.counter--;
                isAppReady = ($scope.counter == 0) && !stayForever;
                if (isAppReady) {
                    _appReady();
                }
                else {
                    $timeout(function () {
                        _checkIfAppReady();
                    }, 1000);
                }
            }

            function _appReady() {
                $state.go('home');
            }


        }
    ]);

    console.log('[OK] - Loading - ' + controllerName);
});
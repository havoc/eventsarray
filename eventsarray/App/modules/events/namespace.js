define([
    '../namespace'
], function (parentNamespace) {
    var namespace = "events";
    return parentNamespace + "." + namespace;
});
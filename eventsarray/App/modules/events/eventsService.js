﻿var deps = ['angular', 'application', './namespace', 'loader'];
define(deps, function (ng, application, namespace, loader) {
    var serviceName = application.resolveServiceName(namespace);

    application.register.service(serviceName, ['$http', '$q',
        function ($http, $q) {

            return {
                getEvents: getEvents
            };

            function getEvents(companyName) {
                var url = '/app/data/events.' + companyName + ".json";
                var request = $http({
                    method: 'GET',
                    url: url
                    // params : {}
                });

                return request.then(handleSuccess, handleError);
            }

            // Private functions

            function handleSuccess(response) {
                return response.data;
            }

            function handleError(response) {
                //console.log('eventsService.handleSuccess', response);
                // Normalize error when encountered with unexpected errors like server errors
                if (!ng.isObject(response) ||
                    !response.data.message) {
                    return $q.reject("An unknown error occured.");
                }

                return $q.reject(response.data.message);
            }

        }
    ]);
});
var deps = [
    'angular',
    'application',
    './namespace',
    '/app/shared/module.require.js',
    'app/modules/events/eventsService'
];
define(deps, function (ng, application, namespace) {
    var controllerName = application.resolveControllerName(namespace);

    application.register.controller(controllerName, ['$scope', '$window', '$timeout', '$ionicScrollDelegate', '$sce', 'app.modules.events.eventsService',
    function controller($scope, $window, $timeout, $ionicScrollDelegate, $sce, eventsService) {

        var screenHeight = $window.screen.height;

        /* Private methods */
        function _init() {
            $scope.flag = {
                emptyContent: true,
                sideMenuOpen: true,
                hubPaneOpen: false,
                cardListFocusedMode: false
            };
            $scope.state = {
                hubPane: {
                    searchEnabled: true
                }
            }
            $scope.iconList = appData.iconList;
            $scope.active = {
                sectionName: '',
                actionIconIndex: -1,
            }
            $scope.searchBar = {
                query: '',
                enabled: true
            }
            $scope.data = {
                cardArray: []
            }
        }

        _init();

        var cardIndex = [];
        function addCardToCardIndexIfNotExist(type, id) {
            id = type + "." + id;
            var exists = cardIndex.indexOf(id) > -1;
            if (!exists) cardIndex.push(id);
            return exists;
        }

        function removeCardFromCardIndex(type, id) {
            id = type + "." + id;
            var index = cardIndex.indexOf(id);
            if (index > -1) cardIndex.splice(index, 1);
        }


        function closeHubPane() {
            $scope.flag.hubPaneOpen = false;
            $scope.active.actionIconIndex = -1;
        }

        /* Filter */
        $scope.nameFilter = function (item) {
            if ($scope.searchBar.query == '') return true;
            var query = $scope.searchBar.query.toLowerCase();
            var result = (item.firstname.toLowerCase().indexOf(query) != -1 || item.lastname.toLowerCase().indexOf(query) != -1);
            return result;
        };

        $scope.getUrl = function (url) {
            return $sce.trustAsResourceUrl(url);
        }

        /* Events */
        $scope.didAppIconClick = function (openAction) {
            $scope.flag.sideMenuOpen = openAction;
            if (openAction == false) closeHubPane(); // Close hub pane also, when intent is to close
        }

        var eventsList = appData.eventsList;
        function fetchEventsData(companyName) {
            eventsService.getEvents(companyName)
                .then(
                    addCompanyEventsCard,
                    function (errorMessage) {
                        console.warn(errorMessage);
                    });
        }

        function addCompanyEventsCard(data) {
            var months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
            function getDate(dateString) {
                var ndate = Number(dateString.substring(3, 5));
                var nmonth = Number(dateString.substring(0, 2)) - 1;
                var year = dateString.substring(6, 10);
                return {
                    date: ndate < 10 ? "0" + ndate.toString() : ndate.toString(),
                    month: months[nmonth],
                    year: year
                };
            };

            var card = {
                type: 'events',
                size: 'narrow',
                id: data.companyName,
                title: data.companyName,
                controls: true,
                hasBarView: false,
                hasNarrowView: true,
                hasWideView: true,
                data: {
                    companyName: data.companyName,
                    eventsList: _.map(data.events, function (event) {
                        event.eventDate = getDate(event.eventDate);
                        return event;
                    })
                }
            }
            $scope.data.cardArray.push(card);
        }

        $scope.didActionIconClick = function (item, $index) {
            //$scope.flag.hubPaneOpen = true;
            $scope.active.actionIconIndex = $index;
            $scope.searchBar.query = '';
            if (item.type == 'company') {
                if (!addCardToCardIndexIfNotExist('events', item.name)) {
                    fetchEventsData(item.name);
                    $scope.flag.emptyContent = false;
                    $scope.didAppIconClick(false);
                }
            }
        }

        $scope.didSearchReset = function () {
            $scope.searchBar.query = '';
            $scope.searchBar.enabled = true;
        }

        $scope.didShowClick = function () {
            $scope.flag.moreOnTop = true;
        }

        $scope.didCardDeleteClick = function (cardItem, cardIndex) {
            removeCardFromCardIndex(cardItem.type, cardItem.id);
            $scope.data.cardArray.splice(cardIndex, 1);
        }
    }
    ]);
    console.log('[OK] - Loading - ' + controllerName);
});
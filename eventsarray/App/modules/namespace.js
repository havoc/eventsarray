define([
    '../namespace'
], function (parentNamespace) {
    var namespace = "modules";
    return parentNamespace + "." + namespace;
});